import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-http-request-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }
`;