import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  body {
    font-family: var(--cells-fontDefault, sans-serif);
    margin: 0;
  }
  body {
    font-family: var(--cells-fontDefault, sans-serif);
    margin: 10px 0px 10px 0px;
  }

  .pre-code {
    background: #f4f4f4;
    border: 1px solid #ddd;
    border-left: 3px solid #f36d33;
    color: #666;
    page-break-inside: avoid;
    font-family: monospace;
    font-size: 15px;
    overflow-y:auto;
    overflow-x:hidden;
    max-height:450px;
    padding: 10px;
    display: block;
    word-wrap: break-word;
    margin:0px 20px 0px 20px; 
  }

    .btnTest {
      display: inline-block;
      padding: 12px 16px;
      vertical-align: middle;
      overflow: hidden;
      text-align: center;
      cursor: pointer;
      white-space: nowrap;
      font-size: 17px;
      background-color: #072146;
      color: #ffffff;
      border-color: #043263;
    }

    .content-buttons-demo {
      width:100%;
      padding:15px;
      text-align:center;
      border-bottom:1px solid #e1e1e1;
      border-top:1px solid #e1e1e1;
      background-color:#f4f4f4;
    }

    h2 {
      text-align:center;
      margin-bottom:5px;
    }
`);
