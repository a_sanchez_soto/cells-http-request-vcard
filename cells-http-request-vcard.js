import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-http-request-vcard-styles.js';
/**
This component ...

Example:

```html
<cells-http-request-vcard></cells-http-request-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsHttpRequestVcard extends LitElement {
  static get is() {
    return 'cells-http-request-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String },
      path: { type: String },
      headers: { type: Object },
      method: { type: String },
      body: { type: Object, },
      contentType: { type: String },
      crossDomain: { type: Boolean },
      withCredentials: { type: Boolean },
      timeout: { type: Number },
      token: { type: String },
      provideTokenType: { type: String },
      requiresToken: { type: Boolean },
      timer: { type: Number },
      updateProgress: { type: Object, },
      transferComplete: { type: Object, },
      transferFailed: { type: Object, },
      transferCanceled: { type: Object, },
      dataType: { type: String },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.timeout = 60000;
    this.dataType = 'json';
    this.token = 'token';
    this.keyToken = 'Authorization';
    this.provideTokenType = 'session';
  }

  addHeaders(key, value) {
    this.headers = this.headers || {};
    this.headers[key] = value;
  }

  buildUrlRequest() {
    return `${this.host}/${this.path}`;
  }

  buildHeaders(request) {
    this.headers = this.headers || {};
    if (!this.headers.Accept) {
      this.headers.Accept = 'application/json, text/plain, */*; q=0.01';
    }
    if (!this.headers['Content-Type']) {
      this.headers['Content-Type'] = 'application/json';
    }
    for (let header in this.headers) {
      if (this.headers.hasOwnProperty(header)) {
        request.setRequestHeader(header, this.headers[header]);
      }
    }
    if (this.requiresToken) {
      if (this.provideTokenType === 'session') {
        request.setRequestHeader(this.keyToken, `Bearer ${window.sessionStorage.getItem(this.token)}`);
      } else {
        request.setRequestHeader(this.keyToken, `Bearer ${window.sessionStorage.getItem(this.token)}`);
      }
    }
    this.requestUpdate();
  }
  generateRequest() {
    let request;
    request = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
      if (this.timeout) {
        this.timer = setTimeout(() => {
          request.abort();
        }, this.timeout);
      }
      request.addEventListener('readystatechange', function() {
        if (request.readyState !== 4) {
          return;
        }
        if (this.timer) {
          clearTimeout(this.timer);
        }
        const validateRequest2 = () => {
          let response = null;
          if (request.response || request.responseText) {
            if (!this.dataType || this.dataType === 'json') {
              response = JSON.parse(request.response || request.responseText);
            } else {
              response = request.response || request.responseText;
            }
          }
          return response;
        };
        const validarRequest = () => {
          if (request.status < 300) {
            if (request.status === 204) {
              resolve({}, request);
            }
            let response = validateRequest2();
            resolve(response, request);
          } else {
            let responseError;
            if (request.status === 404) {
              responseError = {
                'version': 1,
                'severity': 'FATAL',
                'http-status': 404,
                'error-code': 'serviceNotFound',
                'error-message': 'Servicio no disponible.',
                'consumer-request-id': '',
                'system-error-code': 'serviceNotFound',
                'system-error-description': 'Servicio no disponible.',
                'system-error-cause': 'not-found-404'
              };
            } else {
              if (request.response.length > 0) {
                responseError = JSON.parse(request.response);
              } else {
                if (request.timeout === 0) {
                  responseError = {
                    'version': 1,
                    'severity': 'FATAL',
                    'http-status': 504,
                    'error-code': 'timeoutError',
                    'error-message': 'Se ha excedido el tiempo de espera para el servicio.',
                    'consumer-request-id': '',
                    'system-error-code': 'timeoutError',
                    'system-error-description': 'Tiempo de espera del servicio agotado.',
                    'system-error-cause': 'time-out-error-504'
                  };
                }
              }
            }

            const capitalize = (s) => {
              return s.charAt(0).toUpperCase() + s.slice(1);
            };
            const camelTransform = (text) => {
              let arr = text.split('-');
              let textcamel = '';
              arr.forEach((item, index) => {
                if (index === 0) {
                  textcamel = item.toLowerCase();
                } else {
                  textcamel = textcamel.concat(capitalize(item));
                }
              });
              return textcamel;
            };
            let responseTransform = {};
            for (let key in responseError) {
              if (responseError.hasOwnProperty(key)) {
                responseTransform[camelTransform(key)] = responseError[key];
              }
            }
            reject({
              responseError: responseTransform,
              status: request.status,
              statusText: request.statusText,
              request: request
            });
          }
        };
        validarRequest();
      }.bind(this));
      request.open(this.method || 'GET', this.buildUrlRequest(), true);
      this.buildHeaders(request);
      if (this.updateProgress) {
        request.addEventListener('progress', this.updateProgress, false);
      }

      if (this.transferComplete) {
        request.addEventListener('load', this.transferComplete, false);
      }

      if (this.transferFailed) {
        request.addEventListener('error', this.transferFailed, false);
      }

      if (this.transferCanceled) {
        request.addEventListener('abort', this.transferCanceled, false);
      }

      const validateBody = () => {
        if (this.body) {
          let bodyRequest;
          if (typeof this.body === 'object') {
            bodyRequest = JSON.stringify(this.body);
          } else {
            bodyRequest = this.body;
          }
          request.send(bodyRequest);
        } else {
          request.send();
        }
      };
      validateBody();
    });
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-http-request-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsHttpRequestVcard.is, CellsHttpRequestVcard);
